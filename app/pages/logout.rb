class Logout < Page
  def initialize
    @id = PageId.new(
        {
            :web => "#logout-submit",
            :droid => "",
            :ios => ""
        })


    @logout_button = Field.element(
        {
            :web => "#logout-submit",
            :droid => "",
            :ios => ""
        })

    super('Logout')
  end

  def logout
    @logout_button.click
  end


end

PageRegistry.registerPage(Logout)

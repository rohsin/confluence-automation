class Publish < Page
  def initialize
    @id = PageId.new(
        {
            :web => ".rte-button-publish",
            :droid => "",
            :ios => ""
        })

    @publish = Field.element(
        {
            :web => "#rte-button-publish",
            :droid => "",
            :ios => ""
        })

    @page_title = Field.textbox(
                           {
                               :web => "#content-title",
                               :droid => "",
                               :ios => ""
                           }
    )

    @restrictions_button = Field.element(
        {
            :web => "#rte-button-restrictions",
            :droid => "",
            :ios => ""
        })



    super('Publish')
  end

  def publish_page
    sleep(3)
    @publish.exists?
    @publish.click
  end

  def set_title page_title
    @page_title.set_text page_title
  end

  def set_restrictions
    @restrictions_button.exists?
    @restrictions_button.click
  end

end

PageRegistry.registerPage(Publish)

class RestrictionsDialog < Page
  def initialize
    @id = PageId.new(
        {
            :web => ".aui-dialog2-header-main",
            :droid => "",
            :ios => ""
        })

    @restricion_type_selector = Field.element(
        {
            :web => ".s2id_page-restrictions-dialog-selector",
            :droid => "",
            :ios => ""
        })

    @restricion_value_selector = Field.dropdown(
        {
            :web => "#page-restrictions-dialog-selector",
            :droid => "",
            :ios => ""
        })

    @apply_button = Field.element(
        {
            :web => "//button[@id='page-restrictions-dialog-save-button']",
            :droid => "",
            :ios => ""
        })

    super('Restrictions Dialog')
  end

  def value_select
    @restricion_value_selector.exists?
    @restricion_value_selector.select('edit')
  end

  def apply
    @apply_button.click
  end

end

PageRegistry.registerPage(RestrictionsDialog)

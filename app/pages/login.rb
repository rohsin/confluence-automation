class Login < Page
  def initialize
    @id = PageId.new(
        {
            :web => "#google-login",
            :droid => "",
            :ios => ""
        })

    @username = Field.textbox(
        {
            :web => "#username",
            :droid => "",
            :ios => ""
        })

    @password = Field.textbox(
        {
            :web => "#password",
            :droid => "",
            :ios => ""
        })

    @next_button = Field.element(
        {
            :web => "#login-submit",
            :droid => "",
            :ios => ""
        })


    super('Login')
  end

  def sign_in
    @username.set_text('rohitsinghal91@yahoo.com')
    @next_button.click
    @password.set_text('Cabcd@1234')
    @next_button.click
  end

end

PageRegistry.registerPage(Login)

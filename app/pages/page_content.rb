class PageContent < Page
  def initialize
    @id = PageId.new(
        {
            :web => ".iwdhSl",
            :droid => "",
            :ios => ""
        })

    @page_title = Field.element(
        {
            :web => "#title-text",
            :droid => "",
            :ios => ""
        })

    @comment = Field.element(
        {
            :web => ".quick-comment-prompt CommentAddPrompt_root_1fP",
            :droid => "",
            :ios => ""
        })

    @restrictions_button = Field.element(
        {
            :web => "#system-content-items",
            :droid => "",
            :ios => ""
        })

    @edit = Field.element(
        {
            :web => "#editPageLink",
            :droid => "",
            :ios => ""
        })



    super('Page Content')
  end

  def get_page_title
    @page_title.text
  end

  def set_restrictions
    @edit.exists?
    @restrictions_button.click
    sleep(2)
  end

end

PageRegistry.registerPage(PageContent)

class LandingScreen < Page
  def initialize
    @id = PageId.new(
        {
            :web => ".DashboardTab_listTitle_1wR list-title",
            :droid => "",
            :ios => ""
        })

    @create_space = Field.element(
        {
            :web => "#addSpaceLink",
            :droid => "",
            :ios => ""
        })

    @create_page = Field.element(
        {
            :web => "//div[@class='kfFTAe']/div[2]",
            :droid => "",
            :ios => ""
        })

    @recent_updates = Field.element(
        {
            :web => "//div[@class='update-item-title']/a[1]",
            :droid => "",
            :ios => ""
        })

    super('Landing Screen')
  end

  def create_new_page
    @create_page.click
  end

  def last_updated_page
    @recent_updates.click
  end

end

PageRegistry.registerPage(LandingScreen)

class PageCreationDialog < Page
  def initialize
    @id = PageId.new(
        {
            :web => ".dialog-components",
            :droid => "",
            :ios => ""
        })

    @create_button = Field.element(
        {
            :web => "//button[@class='create-dialog-create-button aui-button aui-button-primary']",
            :droid => "",
            :ios => ""
        })

    @selected_template = Field.element(
        {
            :web => "//li[@class='template selected']",
            :droid => "",
            :ios => ""
        })


    super('Page Creation Dialog')
  end

  def create_page
    @selected_template.exists?
    @create_button.exists?
    @create_button.click
  end

end

PageRegistry.registerPage(PageCreationDialog)

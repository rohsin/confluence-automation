Feature: New Page Creation

  @web
  Scenario: As a new user, I want to create a new page
    Given I am on the landing screen
    When I click on create page nav bar button
    And I enter page title "Pursuit of Happyness"
    And I publish the new page created
    Then I should be able to create "Pursuit of Happyness" page
#    And I logout of my account


  @web
  Scenario: As a new user, I want to set restrictions on an existing page
    Given I am on the landing screen
    When I select the last updated page
    Then I set restrictions on the "existing" page
#    And I logout of my account






Then(/^I should be able to create "([^"]*)" page$/) do |page_name|
  expect(App.page_content_page.get_page_title).to eq(page_name)
end

And(/^I enter page title "([^"]*)" and proceed$/) do |page_name|
  App.publish_page.set_title page_name
end

And(/^I enter page title "([^"]*)"$/) do |page_name|
  App.publish_page.set_title page_name
end

When(/^I publish the new page created$/) do
  App.publish_page.publish_page
end


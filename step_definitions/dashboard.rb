When(/^I click on create page nav bar button$/) do
  App.landing_screen_page.create_new_page
  App.page_creation_dialog_page.create_page
end

When(/^I select the last updated page$/) do
  App.landing_screen_page.last_updated_page
end
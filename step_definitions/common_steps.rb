require 'rmagick'

And(/^I set restrictions on the "([^"]*)" page$/) do |page_type|
  page_type=='new' ? App.publish_page.set_restrictions : App.page_content_page.set_restrictions
  App.restrictions_dialog_page.value_select
  App.restrictions_dialog_page.apply
end

And(/^I logout of my account$/) do
  App.logout_page.logout
end

def create_gif scenario_name
# $IMAGE_ARR << "#{$PROJECT_ROOT}/config/end-of-gif-droid.png"
  gif_images = Magick::ImageList.new(*$IMAGE_ARR)
  gif_images.delay = 200
  gif_images.write("results/#{scenario_name}.gif")
end
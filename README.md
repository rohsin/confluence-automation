# confluence-automation


  - An automation framework designed in a way to handle multiple platforms from web, Android and iOS.
  - Behavior-Driven Development(BDD) scenarios are added using cucumber which is an easy way to read and understand domain of the application.

## How to run:

  - rake execute:web[@web]   //This will execute all the test cases marked with tag @web

## Debugging

  - This framework generates a cucumber report file under results diretory
  - Also, a GIF is generated for each scenarios to make debugging an easy task

## Things pending

  - Embedding GIF into the cucumber report with each scenarios
require 'pry'
require_relative '../support/files_collect'


# require File.expand_path(File.dirname(__FILE__) + '/../web/framework/driver_support')
#
#
# $PROJECT_ROOT = File.expand_path(File.join(File.dirname(__FILE__), '../..'))
# Dir["#{$PROJECT_ROOT}/step_definitions/*.rb"].each { |file| require "#{file}" }
# Dir["#{$PROJECT_ROOT}/web/framework/*.rb"].each { |file| require "#{file}" }
FilesCollect.require_common_files
FilesCollect.require_platform_specific_files 'web'
FilesCollect.build_pages
FilesCollect.set_home_page



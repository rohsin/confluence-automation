require 'watir-webdriver'
require 'pry'

def create_driver
  driver_path = "#{FilesCollect::ROOT}/drivers/chromedriver"
  driver = Selenium::WebDriver.for :chrome, driver_path: driver_path
  browser = Watir::Browser.new(driver)
  at_exit do
    sleep(2)
    browser.quit
  end
  browser
end


# B = create_driver

Before do |scenario|
  B = create_driver
  $IMAGE_ARR=[]
  B.cookies.clear
  B.goto 'https://rohitsinghal.atlassian.net/login'
  B.wait
  Watir::Wait.until { B.ready_state == 'complete' }
end

After do |scenario|
  file_name = scenario.name.gsub(/\W/, '')+".png"
  B.driver.save_screenshot "results/" + file_name
  $IMAGE_ARR << "results/" + file_name
  scenario_slug = scenario.name.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  generate_gif scenario_slug
  B.close
end


def generate_gif scenario_slug
  create_gif scenario_slug
#embedding generated gif to cucumber report
end




